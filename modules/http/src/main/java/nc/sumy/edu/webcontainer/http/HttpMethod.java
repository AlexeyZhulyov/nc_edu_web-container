package nc.sumy.edu.webcontainer.http;

/**
 * Enum that  contain HttpMethod-data.
 * @author Vinogradov Maxim
 */
public enum HttpMethod {
    GET,
    OPTIONS,
    POST,
    UNKNOWN
}
