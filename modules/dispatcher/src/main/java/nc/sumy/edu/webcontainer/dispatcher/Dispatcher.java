package nc.sumy.edu.webcontainer.dispatcher;

import nc.sumy.edu.webcontainer.http.HttpResponse;

public interface Dispatcher {

    HttpResponse getResponse();

}
