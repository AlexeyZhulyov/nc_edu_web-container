package nc.sumy.edu.webcontainer.configuration;

public interface Configuration {
    int getPort();

    void setPort(int port);

}
