# Java-based web container for educational purposes 
![Travis CI Badge](https://travis-ci.org/AlexeyZhulyov/nc_edu_web-container.svg?branch=master "Last build on Travis CI ")
[![Codacy Badge](https://api.codacy.com/project/badge/grade/66691ec6b84b40fa8019a83da4845ea2)](https://www.codacy.com/app/dgroup/nc_edu_web-container)
[![Coverage Badge](https://coveralls.io/repos/github/AlexeyZhulyov/nc_edu_web-container/badge.svg?branch=master)](https://coveralls.io/github/AlexeyZhulyov/nc_edu_web-container?branch=master).
